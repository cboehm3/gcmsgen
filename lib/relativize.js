var jquery = require('metalsmith-jquery')
var path = require('path').posix

module.exports = jquery('**/*.{htm,html}', ($, file) => {
    let dir = path.join('/', path.dirname(file))
    let attrs = ['data', 'href', 'poster', 'src']
    for (let attr of attrs) {
        $(`[${attr}]`).each(function() {
            let link = $(this).attr(attr)
            if (link.startsWith('/')) {
                // TODO: handle this properly
                if (link === '/') link = '/index.html'
                let rel = path.relative(dir, link)
                if (rel === '') rel = 'index.html'
                $(this).attr(attr, rel)
            }
        })
    }
}, {
    decodeEntities: false
})
