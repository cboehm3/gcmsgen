var handlebars = require('handlebars')
var fs = require('fs')
var path = require('path')
var pify = require('pify')

var readdir = pify(fs.readdir)
var readFile = pify(fs.readFile)

module.exports = function*(files, metalsmith) {
    // Load all files having a `partial` key, which can be either the partial
    // name, or `true`, in which case the name is the file name up to first dot
    // (since dots are not compatible with handlebars syntax).
    for (let f in files) {
        let partial = files[f].partial
        if (!partial) continue
        if (partial === true)
            partial = path.basename(f).replace(/\..*/, '')
        handlebars.registerPartial(partial, files[f].contents.toString())
        // TODO: use `ignore` property instead?
        delete files[f]
    }

    // Load all files from lib/templates and strip `.*` from filename. Those
    // starting with `_` are registered as partials, with name taken from the
    // filename sans _. The others are compiled and stored in
    // metadata().layouts.
    let meta = metalsmith.metadata()
    meta.layouts = meta.layouts || {}

    let template_dir = path.join(__dirname, '..', 'templates')
    let templates = {}
    for (let fname of yield readdir(template_dir)) {
        templates[fname] = readFile(path.join(template_dir, fname))
    }
    templates = yield templates

    for (let fname in templates) {
        let lname = fname.replace(/\..*/, '')
        let data = templates[fname].toString()
        if (lname.startsWith('_'))
            handlebars.registerPartial(lname.slice(1), data)
        else
            meta.layouts[lname] = handlebars.compile(data)
    }
}
