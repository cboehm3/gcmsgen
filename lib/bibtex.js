var citeproc = require('citeproc')
var converter = require('biblatex-csl-converter')
var fs = require('fs')
var handlebars = require('handlebars')
var path = require('path')
var pify = require('pify')

var readdir = pify(fs.readdir)
var readFile = pify(fs.readFile)

function getname(files, f, prop) {
    let name = files[f][prop]
    if (name === true)
        name = path.basename(f).replace(/\..*/, '')
    return name
}

module.exports = function*(files, metalsmith) {
    let meta = metalsmith.metadata()
    meta.bibtex = meta.bibtex || {}

    let builtins = { styles: {}, locales: {} }

    let style_dir = path.join(__dirname, '..', 'csl', 'styles')
    for (let fname of yield readdir(style_dir)) {
        let name = fname.replace(/\..*/, '')
        builtins.styles[name] = readFile(path.join(style_dir, fname))
            .then(contents => citeproc.parseXml(contents.toString()))
    }

    let locale_dir = path.join(__dirname, '..', 'csl', 'locales')
    for (let fname of yield readdir(locale_dir)) {
        let name = fname.replace(/\..*/, '')
        builtins.locales[name] = readFile(path.join(locale_dir, fname))
            .then(contents => citeproc.parseXml(contents.toString()))
    }

    builtins = yield builtins
    meta.bibtex.styles = builtins.styles
    meta.bibtex.locales = builtins.locales

    meta.bibtex.collections = {}

    for (let f in files) {
        let name = getname(files, f, 'bibliography')
        if (name) {
            let bib = new converter.BibLatexParser(files[f].contents.toString(), {
                processUnknown: true
            }).output
            let csl = new converter.CSLExporter(bib).output
            //console.log(csl)
            let collection = {}
            for (let n in csl) {
                let id = bib[n].entry_key
                collection[id] = csl[n]
                collection[id].id = id
            }
            meta.bibtex.collections[name] = collection
            continue
        }

        name = getname(files, f, 'csl-style')
        if (name) {
            meta.bibtex.styles[name] = citeproc.parseXml(files[f].contents.toString())
            continue
        }

        name = getname(files, f, 'csl-locale')
        if (name) {
            meta.bibtex.locales[name] = citeproc.parseXml(files[f].contents.toString())
            continue
        }
    }

    handlebars.registerHelper('bibliography', function(collection, options) {
        let style = options.hash.style || meta.bibtex['default-style']
        let lang = options.hash.lang
        let include = options.hash.include

        if (typeof collection === 'string')
            collection = meta.bibtex.collections[collection]

        if (typeof style === 'string')
            style = meta.bibtex.styles[style]

        let sys = {
            retrieveLocale: lang => meta.bibtex.locales[lang],
            retrieveItem: id => collection[id],
        }
        // Always force lang if it is set
        let engine = new citeproc.Engine(sys, style, lang, lang)
        engine.opt.development_extensions.wrap_url_and_doi = true

        if (!options.fn) {
            // We're not run as a block helper -> return full bibliography
            include = include || Object.keys(collection)
            engine.updateItems(include)
            let [params, items] = engine.makeBibliography({
            //    select: [{ field: 'type', value: 'thesis' }]
            })
            return params.bibstart + items.join('\n') + params.bibend
        }

        let citations = {}

        let ctx = Object.assign({}, this)
        ctx.cite = function(...ids) {
            let opts = ids.pop()
            citation = {
                citationItems: [],
                properties: {},
            }
            for (let id of ids) citation.citationItems.push({id: id})
            let refs = engine.appendCitationCluster(citation)
            for (let [_, ref, id] of refs) citations[id] = ref
            let id = refs[refs.length - 1][2]
            return `!!!${id}!!!`
        }

        if (include) engine.updateUncitedItems(include)

        let block = options.fn(ctx)
            .replace(/!!!([a-z0-9]*)!!!/g, (_, id) => citations[id])

        let [params, items] = engine.makeBibliography()
        return block + params.bibstart + items.join('\n') + params.bibend
    })
}
