var utils = require('handlebars').Utils

module.exports = {
    and: (...args) => {
        let opts = args.pop()
        let arg
        for (arg of args)
            if (utils.isEmpty(arg))
                return (opts.inverse ? opts.inverse(this) : undefined)
        return (opts.fn ? opts.fn(arg) : arg)
    },

    or: (...args) => {
        let opts = args.pop()
        for (let arg of args)
            if (!utils.isEmpty(arg))
                return (opts.fn ? opts.fn(arg) : arg)
        return (opts.inverse ? opts.inverse(this) : undefined)
    },

    eq: (a, b) => a === b,
    eqw: (a, b) => a == b,

    not: a => !a,

    list: (...args) => {
        args.pop()
        return args
    },
}

