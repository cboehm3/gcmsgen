var ancestry = require('metalsmith-ancestry')
var merge = require('lodash.merge')
var handlebars = require('handlebars')
var markdownit = require('metalsmith-markdownit')
var markdownit_decorate = require('markdown-it-decorate')
var markdownit_mathjax = require('markdown-it-mathjax')
var ware = require('ware')

var pipeline = ware()

// Run handlebars on all source files that have the `template` key set

    .use(function*(files, metalsmith) {
        for (let f in files) {
            let data = files[f]
            if (data.template) {
                let template = handlebars.compile(data.contents.toString())
                data.contents = Buffer.from(template(merge({}, metalsmith.metadata(), data)))
            }
        }
    })

// Convert markdown to html

    .use(markdownit({ html: true })
         .use(markdownit_decorate)
         .use(markdownit_mathjax()))

// Include sitemap information for breadcrumbs and sidebar. This has to be done
// after markdown to get the actual tree for the html files; we do not want to
// accidentally link to some md in the page navigation.

    .use(function*(files, metalsmith) {
        // Include file path as `path` key, so that ancestry can sort by it
        for (let f in files)
            files[f].path = f
    })

    .use(ancestry({
        ancestryProperty: 'tree',
        sortBy: ['order', 'path'],
    }))

// Run all files having a `layout` key through handlebars

    .use(function*(files, metalsmith) {
        let meta = metalsmith.metadata()
        for (let f in files) {
            let data = files[f]
            let l = data.layout
            if (!l) continue
            let compiled = meta.layouts[l]
            if (!compiled)
                throw new Error('Layout ' + l + ' in file ' + f + ' not found.')
            data.contents = Buffer.from(compiled(merge({}, meta, data)))
        }
    })

module.exports = pipeline.run.bind(pipeline)
