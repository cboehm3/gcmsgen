var clone = require('lodash.clonedeep')
var merge = require('lodash.merge')
var path = require('path')
var pify = require('pify')
var run = pify(require('./template-pipeline'))
var utils = require('handlebars').Utils

module.exports = function*(files, metalsmith) {
    let langs = Object.keys(metalsmith.metadata().langs)
    let basefiles = {}
    let translations = {}

    for (let f in files) {
        let data = files[f]
        delete files[f]
        if (!data.lang) {
            basefiles[f] = data
            continue
        }
        let single_lang = (typeof data.lang === 'string')
        if (!data.basename) {
            let fname = path.basename(f)
            data.basename = single_lang
                ? fname.replace(`.${data.lang}.`, '.')
                : fname
        }
        let newpath = path.join(path.dirname(f), data.basename)
        translations[newpath] = translations[newpath] || {}
        if (single_lang) {
            translations[newpath][data.lang] = data
            data.translations = translations[newpath]
            data.langinfo = metalsmith.metadata().langs[data.lang]
        } else if (utils.isArray(data.lang)) {
            for (lang of data.lang) {
                let d = clone(data)
                d.lang = lang
                translations[newpath][lang] = d
                d.translations = translations[newpath]
                d.langinfo = metalsmith.metadata().langs[lang]
            }
        } else {
            for (let lang in data.lang) {
                let d = clone(data)
                merge(d, data.lang[lang])
                d.lang = lang
                translations[newpath][lang] = d
                d.translations = translations[newpath]
                d.langinfo = metalsmith.metadata().langs[lang]
            }
        }
    }

    // TODO: missing page placeholders

    let pipelines = [run(basefiles, metalsmith).then(result => merge(files, result))]
    for (let lang of langs) {
        let langfiles = {}
        for (let f in translations) {
            if (translations[f][lang])
                langfiles[path.join(lang, f)] = translations[f][lang]
            else
                console.log(`Missing translation (${lang}) for file ${f}`)
        }
        let pipeline = run(langfiles, metalsmith)
            .then(result => merge(files, result))
        pipelines.push(pipeline)
    }
    yield pipelines
}

