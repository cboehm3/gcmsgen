var cheerio = require('cheerio')
var clone = require('lodash.clonedeep')
var path = require('path').posix

module.exports = function*(files, metalsmith) {
    let indexlang = metalsmith.metadata().indexlang
    files['index.html'] = clone(files[path.join(indexlang, 'index.html')])
    let $ = cheerio.load(files['index.html'].contents, { decodeEntities: false })
    $('head').prepend(`<base href="${indexlang}/">`)
    files['index.html'].contents = Buffer.from($.html())
}
