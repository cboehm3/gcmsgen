var merge = require('lodash.merge')
var fs = require('fs')
var minimist = require('minimist')
var multimatch = require('multimatch')
var path = require('path')
var pify = require('pify')
var yaml = require('js-yaml')

var readFile = pify(fs.readFile)

function process_yaml(files, dir, contents) {
    if (dir === '.') dir = ''
    // Object keys are not sorted, so it is not possible to specify e.g.
    //     "*.md":
    //        foo: "override"
    //     "*":
    //        foo: "default"
    // since the order of application is undefined. Support multi-document
    // yaml for this purpose:
    //     "*.md":
    //        foo: "override"
    //     ---
    //     "*":
    //        foo: "default"
    yaml.safeLoadAll(contents, doc => {
        for (let pattern in doc) {
            for (let f of multimatch(Object.keys(files), path.join(dir, pattern))) {
                files[f] = merge({}, doc[pattern], files[f])
            }
        }
    })
}

module.exports = function*(files, metalsmith) {
    // Apply metadata files ordered from more to less specific (regarding the
    // directory structure). Each is recursively merged with the existing
    // metadata of the respective file, with existing values taking precedence.
    for (let f of multimatch(Object.keys(files), '**/defaults.yaml')
             .sort((a, b) => a.length < b.length))
        process_yaml(files, path.dirname(f), files[f].contents)

    // Finally, built-in defaults are the least specific
    try {
        let defaults = yield readFile(path.join(__dirname, '..', 'defaults.yaml'))
        process_yaml(files, '', defaults)
    } catch (e) {
        console.log('Warning: failed to load defaults.yaml: ' + e.message)
    }

    // Load metadata from meta.yaml and src/meta.yaml
    try {
        let config = yield readFile(path.join(__dirname, '..', 'meta.yaml'))
        merge(metalsmith.metadata(), yaml.safeLoad(config))
    } catch (e) {
        console.log('Warning: failed to load meta.yaml: ' + e.message)
    }

    if (files['meta.yaml']) {
        merge(metalsmith.metadata(), yaml.safeLoad(files['meta.yaml'].contents))
        delete files['meta.yaml']
    }

    // Finally, merge with values passed via command line
    merge(metalsmith.metadata(), minimist(process.argv.slice(2)))
}
